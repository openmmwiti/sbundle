package com.beast.sbundle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import com.beast.sbundle.exceptions.MissingPostfixException;
import com.beast.sbundle.exceptions.MissingSegmentException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mutai on 5/20/16.
 */

public class SmsListener extends BroadcastReceiver {
    private final static String TAG = "SBundle";
    private static long lastRequestTime;
    private static List<Long> firstTimestamps = new ArrayList<>();
    private static Context ctx;
    private static boolean pendingRequest = false;
    private static final String SENDER = "Safaricom";
    private static final String NUMBER = "144";

    public static void requestDetails(){
        if(!pendingRequest){
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(NUMBER, null, "", null, null);
            SmsListener.lastRequestTime = System.currentTimeMillis();
            Log.d(TAG, Long.toString(lastRequestTime));
            pendingRequest = true;
        }
        else{
            updateUi("Please wait...pending request in progress.");
        }
    }

    private static void updateUi(String message){
        Intent detailsIntent = new Intent("com.beast.sbundle.DETAILS");
        Log.d(TAG, message);
        detailsIntent.putExtra("details", message);
        ctx.sendBroadcast(detailsIntent);
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        ctx = context;
        new Runnable(){
            @Override
            public void run() {
                final Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    final Object[] pdusObj = (Object[]) bundle.get("pdus");
                    for (int i = 0; i < pdusObj.length; i++) {
                        SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        String senderAddress = msg.getOriginatingAddress();
                        if(senderAddress.equals(SENDER)){
                            abortBroadcast();
                            long timestamp = msg.getTimestampMillis();
                            String message = msg.getMessageBody();
                            if(message.startsWith("Current balance:") &&
                                message.contains("KSH.") &&
                                message.contains(".Expiry date:") &&
                                message.contains(".Tariff:"))
                            {
                                Log.d(TAG + "First ", message + " Time: " + timestamp);
                                firstTimestamps.add(timestamp);
                                if (timestamp > lastRequestTime && timestamp >= firstTimestamps.get(firstTimestamps.size() - 1)) {
                                    DetailsState.updateBalance(message, timestamp);
                                }
                            }
                            else {
                                if(firstTimestamps.contains(timestamp)){
                                    if ('.' == (message.charAt(message.length() - 1))) {
                                        pendingRequest = false;
                                    }
                                    Log.d(TAG + "Future ", message + " Time: " + timestamp);
                                    if (timestamp > lastRequestTime && timestamp >= firstTimestamps.get(firstTimestamps.size() - 1)) {
                                        DetailsState.updateOthers(message, timestamp);
                                    }
                                }
                                else{
                                    clearAbortBroadcast();
                                }
                            }
                        }
                    }
                }
            }
        }.run();
    }

    private static class DetailsState{
        private static final DetailList detailList = new DetailList(){
            {
                for (Items item : Items.values()){
                    add(new DCB(item));
                }
            }
        };
        private final static String AIRTIME_PREFIX = "balance: ";
        private final static String AIRTIME_POSTFIX = " KSH";
        private final static String AIRTIME_EXP_PREFIX = "date: ";
        private final static String AIRTIME_EXP_POSTFIX = ".";
        private final static String BUNDLE_POSTFIX = " MB Data Bundle";
        private final static String BONGA_POSTFIX = " points Bonga";
        private final static String FREE_AIRTIME_POSTFIX = " KSH Free Airtime";
        private final static String OKOA_JAHAZI_POSTFIX = " KSH Okoa jahazi";
        private final static String ONNET_TALKTIME_POSTFIX = " Minutes On-net Talktime";
        private final static String ONNET_SMS_POSTFIX = " Items On-net SMS";
        private final static String BONGA_SMS_POSTFIX = " Items Bonga SMS";
        private final static String DAILY_BUNDLE_POSTFIX = " MB Daily Internet Bundle";
        private final static String BONGA_BUNDLE_POSTFIX = " MB Bonga Data Bundle";

        //rare
        private final static String MPESA_BONUS_AIRTIME_POSTFIX = " KSH Mpesa Bonus Airtime";

        private static String message;
        private static long timestamp;

        private static void updateDetail(Items item, String value){
            detailList.updateItem(item, timestamp, value);
            updateUi(detailList.getMessage(lastRequestTime));
        }

        public static void updateBalance(String msg, long tm){
            timestamp = tm;
            updateDetail(Items.AIRTIME, getMiddleString(msg, AIRTIME_PREFIX, AIRTIME_POSTFIX));
            updateDetail(Items.AIRTIME_EXPIRY, getMiddleString(msg, AIRTIME_EXP_PREFIX, AIRTIME_EXP_POSTFIX));
        }

        public static void updateOthers(String msg, long tm){
            timestamp = tm;
            message = msg;
            if(message.contains(BUNDLE_POSTFIX)){
                tryUpdateBundle();
            }
            if(message.contains(BONGA_POSTFIX)){
                tryUpdateBonga();
            }
            if(message.contains(FREE_AIRTIME_POSTFIX)){
                tryUpdateFreeAirtime();
            }
            if(message.contains(OKOA_JAHAZI_POSTFIX)){
                tryUpdateOkoaJahazi();
            }
            if(message.contains(ONNET_TALKTIME_POSTFIX)){
                tryUpdateOnnetTalktime();
            }
            if(message.contains(ONNET_SMS_POSTFIX)){
                tryUpdateOnnetSms();
            }
            if(message.contains(BONGA_SMS_POSTFIX)){
                tryUpdateBongaSms();
            }
            if(message.contains(DAILY_BUNDLE_POSTFIX)){
                tryUpdateDailyBundle();
            }
            if (message.contains(BONGA_BUNDLE_POSTFIX)){
                tryUpdateBongaBundle();
            }
            if(message.contains(MPESA_BONUS_AIRTIME_POSTFIX)){
                tryUpdateMPesaBonusAirtime();
            }
        }

        private static void tryUpdateBundle(){
            try {
                updateDetail(Items.BUNDLE, getFromPostfix(BUNDLE_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get bundle: " + e.getMessage());
            }
        }

        private static void tryUpdateBonga(){
            try {
                updateDetail(Items.BONGA_POINTS, getFromPostfix(BONGA_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get bonga points: " + e.getMessage());
            }
        }

        private static void tryUpdateFreeAirtime(){
            try {
                updateDetail(Items.FREE_AIRTIME, getFromPostfix(FREE_AIRTIME_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get free airtime: " + e.getMessage());
            }
        }

        private static void tryUpdateOkoaJahazi(){
            try {
                updateDetail(Items.OKOA_JAHAZI, getFromPostfix(OKOA_JAHAZI_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get okoa jahazi: " + e.getMessage());
            }
        }

        private static void tryUpdateOnnetTalktime(){
            try {
                updateDetail(Items.ONNET_TALKTIME, getFromPostfix(ONNET_TALKTIME_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get on-net talktime: " + e.getMessage());
            }
        }

        private static void tryUpdateOnnetSms(){
            try {
                updateDetail(Items.ONNET_SMS, getFromPostfix(ONNET_SMS_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get on-net sms: " + e.getMessage());
            }
        }

        private static void tryUpdateBongaSms(){
            try {
                updateDetail(Items.BONGA_SMS, getFromPostfix(BONGA_SMS_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get bonga sms: " + e.getMessage());
            }
        }

        private static void tryUpdateDailyBundle(){
            try {
                updateDetail(Items.DAILY_BUNDLE, getFromPostfix(DAILY_BUNDLE_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get daily bundle: " + e.getMessage());
            }
        }

        private static void tryUpdateBongaBundle(){
            try {
                updateDetail(Items.BONGA_BUNDLE, getFromPostfix(BONGA_BUNDLE_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get bonga bundle: " + e.getMessage());
            }
        }

        private static void tryUpdateMPesaBonusAirtime(){
            try {
                updateDetail(Items.MPESA_BONUS_AIRTIME, getFromPostfix(MPESA_BONUS_AIRTIME_POSTFIX));
            }
            catch (Exception e){
                Log.d(TAG, "Unable to get mpesa bonus airtime: " + e.getMessage());
            }
        }

        private static String getFromPostfix(final String postfix) throws MissingSegmentException, MissingPostfixException{
            try {
                int end = message.indexOf(postfix);
                String draft = message.substring(0, end);
                for (int i = draft.length() - 1; i > -1; i--){
                    if (draft.charAt(i) == ' ' || draft.charAt(i) == ','){
                        return draft.substring(i + 1);
                    }
                }
                throw new MissingSegmentException("Message incomplete due to missing segment on the left.");
            }
            catch (RuntimeException e){
                throw new MissingPostfixException("Message does not include postfix: " + postfix);
            }
        }

        private static String getMiddleString(String string, String prefix, String postfix) {
            String result = "";
            // Cut the beginning of the text to not occasionally meet a
            // 'textTo' value in it:
            result = string.substring(
                            string.indexOf(prefix) + prefix.length(),
                            string.length()
            );
            // Cut the excessive ending of the text:
            result = result.substring(
                            0,
                            result.indexOf(postfix)
            );
            return result;
        }
    }
}
//Possibility of having to check if message may be result of external actions
