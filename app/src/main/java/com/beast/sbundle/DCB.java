package com.beast.sbundle;

/**
 * Created by mutai on 5/25/16.
 */
public class DCB implements Comparable<DCB>{
    Items item;
    String value;
    long timestamp;
    long localTimestamp;

    @Override
    public int compareTo(DCB other) {
        return (int)(this.localTimestamp - other.localTimestamp);
    }

    public DCB(Items item){
        this.item = item;
    }

    public Items getItem(){
        return this.item;
    }

    public void setValue(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

    public long getTimestamp(){
        return this.timestamp;
    }

    public long getLocalTimestamp(){
        return this.localTimestamp;
    }
}
