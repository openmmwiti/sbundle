package com.beast.sbundle;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mutai on 5/20/16.
 */
public class DetailMessage implements Parcelable {

    List<String> messages = new ArrayList<>();

    private DetailMessage(Parcel in){
        messages = (ArrayList<String>)in.readSerializable();
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(messages);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        @Override
        public DetailMessage[] newArray(int size) {
            return new DetailMessage[size];
        }

        @Override
        public DetailMessage createFromParcel(Parcel in) {
            return new DetailMessage(in);
        }
    };
}
