package com.beast.sbundle;

/**
 * Created by mutai on 5/25/16.
 */
public enum Items {
    AIRTIME,
    AIRTIME_EXPIRY,
    BUNDLE,
    BONGA_POINTS,
    FREE_AIRTIME,
    OKOA_JAHAZI,
    ONNET_TALKTIME,
    ONNET_SMS,
    BONGA_SMS,
    DAILY_BUNDLE,
    BONGA_BUNDLE,
    MPESA_BONUS_AIRTIME
}
