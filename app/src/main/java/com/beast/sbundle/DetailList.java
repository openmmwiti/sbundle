package com.beast.sbundle;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by mutai on 5/26/16.
 */
public class DetailList extends ArrayList<DCB> {

    private String message;

    public void updateItem(Items item, long timestamp, String value){
        for (DCB dcb: this){
            if(dcb.item == item){
                dcb.value = value;
                dcb.timestamp = timestamp;
                dcb.localTimestamp = System.currentTimeMillis();
            }
        }
        Collections.sort(this);
    }

    private void updateMessage(long lastRequestTime){
        message = "";
        DCB dcb;
        for(int i = 0; i < this.size(); i++){
            dcb = this.get(i);
            if(dcb.item == Items.AIRTIME){
                if(dcb.getTimestamp() > lastRequestTime){
                    message += dcb.value + " " + dcb.getItem() + "\n";
                    for ( i += 1; i < this.size(); i++){
                        dcb = this.get(i);
                        if(dcb.value != null){
                            message += dcb.value + " " + dcb.getItem() + "\n";
                        }
                    }
                }
                else{
                    break;
                }
            }
        }
    }

    public String getMessage(long lastRequestTime){
        updateMessage(lastRequestTime);
        return message;
    }
}
