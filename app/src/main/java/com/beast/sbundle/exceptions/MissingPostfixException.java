package com.beast.sbundle.exceptions;

/**
 * Created by mutai on 5/23/16.
 */
public class MissingPostfixException extends Exception {
    public MissingPostfixException(String message){
        super(message);
    }
}
