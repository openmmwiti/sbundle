package com.beast.sbundle.exceptions;

/**
 * Created by mutai on 5/23/16.
 */
public class MissingSegmentException extends Exception {
    public MissingSegmentException(String message){
        super(message);
    }
}
