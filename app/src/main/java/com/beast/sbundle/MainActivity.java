package com.beast.sbundle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private String messages;
    private final static String TAG = "SBundle";
    private static boolean initialized = false;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            abortBroadcast();
            final Bundle bundle = intent.getExtras();
            messages = bundle.getString("details");
            showDetails();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter intentFilter = new IntentFilter("com.beast.sbundle.DETAILS");
        intentFilter.setPriority(999);
        this.registerReceiver(receiver, intentFilter);
        getDetails();
        initialized = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!initialized){
            getDetails();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        initialized = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }

    protected void getDetails(){
        setContentView(R.layout.activity_main);
        TextView view = (TextView)findViewById(R.id.detailsView);
        view.setText(R.string.wait_string);
        SmsListener.requestDetails();
    }

    private void showDetails(){
        setContentView(R.layout.activity_main);
        TextView view = (TextView)findViewById(R.id.detailsView);
        if(messages.isEmpty()){
            view.setText(R.string.fetch_fail);
        }
        else{

            view.setText(messages);
        }
    }

    private void unregisterReceiver(){
        try{
                unregisterReceiver(this.receiver);
        }catch (IllegalArgumentException e){

        }
    }
}
